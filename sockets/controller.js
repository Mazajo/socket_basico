const socketController = (socket) => {
    console.log('cliente conectado');

    socket.on('disconnect', () => {
        console.log('Cliente desconectado')
    });

    socket.on('enviar-mensaje', (payload) => {

        socket.broadcast.emit('enviar-mensaje', payload)


    });
}
module.exports = {
    socketController
}
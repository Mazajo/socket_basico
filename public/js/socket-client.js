const socket = io();


const lblonline = document.querySelector('#lblonline');
const lbloffline = document.querySelector('#lbloffline');
const txtMensaje = document.querySelector('#txtMensaje');
const btnEnviar = document.querySelector('#btnEnviar');

socket.on('connect', () => {
    console.log('Cliente conectado');

    lbloffline.style.display = 'none';
    lblonline.style.display = '';

});

socket.on('disconnect', () => {
    console.log('Cliente desconectado');

    lbloffline.style.display = '';
    lblonline.style.display = 'none';
});


socket.on('enviar-mensaje', (payload) => {
    console.log(payload);
});

btnEnviar.addEventListener('click', () => {
    const mensaje = txtMensaje.value;
    const payload = {
        mensaje,
        nombre: 'Jorge',
        id: '123456',
        fecha: new Date().getTime()
    }
    socket.emit('enviar-mensaje', payload);
});